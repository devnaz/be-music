from django.contrib import admin
from .models import *

# Register your models here.
class SongInline(admin.TabularInline):
    model = Song
    extra = 2


class AlbumAdmin(admin.ModelAdmin):
    inlines = [SongInline]


admin.site.register(Artist)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Song)
