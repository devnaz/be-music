from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from .models import Artist, Album, Song

# Create your views here.

def index(request):
    artists = Artist.objects.all() #.order_by('-pk')

    if request.GET:
        text = request.GET.get('search')
        artists = Artist.objects.filter(name__icontains=text)

    context = {
        'artists': artists,
    }
    return render(request, 'music/singer_list.html', context)


def album_list(request, name=None):
    name = name.replace('-', ' ')
    artist = get_object_or_404(Artist, name=name)
    # albums = Album.objects.all().filter(artist=artist.pk)
    url = name.replace(' ', '-')
    return render(request, 'music/album_list.html', {'new_url': url, 'artist': artist})


def song_list(request, singer_name=None, album_name=None):
    singer_name = singer_name.replace('-', ' ')
    artist = get_object_or_404(Artist, name=singer_name)
    album_name = album_name.replace('-', ' ')
    album = get_object_or_404(Album, title=album_name)
    # songs = Song.objects.filter(album=album.pk)
    return render(request, 'music/song_list.html', {'album': album})
