from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),  # show all singers

    url(r'^(?P<name>[\w.-]+)/$', views.album_list, name='album_list'),  # albums's singer
    url(r'^(?P<singer_name>[\w.-]+)/(?P<album_name>[\w.-]+)/$', views.song_list, name='song_list'),   # songs of the album

    # url(r'^(?P<singer_name>[\w.]+)/(?P<album_name>[\w.]+)/$', views.song_list),   # songs of the album
]
