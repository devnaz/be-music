from django.db import models

# Create your models here.
def upload_artist_photo(instance, filename):
    return "images/artists/{}/{}".format(instance.name, filename)


def upload_logo(instance, filename):
    artist = instance.artist
    return "images/album_logos/{0}/{1}".format(artist, filename)


def upload_song(instance, filename):
    return "songs/%s/%s" %(instance.album, filename)


def generate_url2(stroke):
    name = stroke
    url = name.replace(' ', '-')
    return url


class Artist(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    photo = models.ImageField(upload_to=upload_artist_photo)

    def __str__(self):
        return self.name

    def generate_url(self):
        return generate_url2(self.name)


class Album(models.Model):
    title = models.CharField(max_length=100)
    genre = models.CharField(max_length=50)
    logo = models.ImageField(upload_to=upload_logo)
    released = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    # on delete - it means when class Album will be deleted, all songs which was related with the album will be deleted too
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='albums')

    def __str__(self):
        return "%s - %s" %(self.artist, self.title)

    def generate_url(self):
        return generate_url2(self.title)


class Song(models.Model):
    title = models.CharField(max_length=150)
    music = models.FileField(upload_to=upload_song)
    # on delete - it means when class Album will be deleted, all songs which was related with the album will be deleted too
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='songs')  # we can write so album.songs.all instead album_set.all

    def __str__(self):
        return self.title
